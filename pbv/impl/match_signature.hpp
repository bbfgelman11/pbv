#ifndef PBV_IMPL_MATCH_SIGNATURE_HPP
#define PBV_IMPL_MATCH_SIGNATURE_HPP

#include "convertible_from.hpp"
#include "invoke_result.hpp"
#include "type_list.hpp"
#include <concepts>

namespace pbv {
namespace impl {
namespace match_signature_detail {

template <bool>
struct Tag;

template <>
struct Tag<false> {};

template <>
struct Tag<true> : Tag<false> {};

template <typename>
inline constexpr Tag<true> Tag_v{};

template <typename Arg, bool TagVal>
struct Tagged {};

template <typename ArgsList, typename TaggedList = TypeList<>>
struct MatchSignatureImpl;

template <typename... Args, bool... TagVals>
struct MatchSignatureImpl<TypeList<>, TypeList<Tagged<Args, TagVals>...>> {
    template <typename R, typename Class>
    static constexpr auto
    match(R (Class::*mem_fun)(Args...), Tag<TagVals>...) {
        return mem_fun;
    }
};

template <typename... Bases>
struct Inherit : Bases... {
    using Bases::match...;
};

template <typename First, typename... Rest, typename... Accum>
struct MatchSignatureImpl<TypeList<First, Rest...>, TypeList<Accum...>>
    : Inherit<
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First&&, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First const&, false>>>> {};

template <typename First, typename... Rest, typename... Accum>
struct MatchSignatureImpl<TypeList<First&&, Rest...>, TypeList<Accum...>>
    : Inherit<
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First&&, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First const&, false>>>> {};

template <typename First, typename... Rest, typename... Accum>
requires convertible_from<First, First&&>
struct MatchSignatureImpl<TypeList<First&&, Rest...>, TypeList<Accum...>>
    : Inherit<
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First&&, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First const&, false>>>> {};

template <typename First, typename... Rest, typename... Accum>
struct MatchSignatureImpl<TypeList<First&, Rest...>, TypeList<Accum...>>
    : Inherit<
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First&, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First const&, false>>>> {};

template <typename First, typename... Rest, typename... Accum>
requires convertible_from<First, First&>
struct MatchSignatureImpl<TypeList<First&, Rest...>, TypeList<Accum...>>
    : Inherit<
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First, false>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First&, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First const&, false>>>> {};

template <typename First, typename... Rest, typename... Accum>
struct MatchSignatureImpl<TypeList<First const&, Rest...>, TypeList<Accum...>>
    : MatchSignatureImpl<
          TypeList<Rest...>,
          TypeList<Accum..., Tagged<First const&, true>>> {};

template <typename First, typename... Rest, typename... Accum>
requires convertible_from<First, First const&>
struct MatchSignatureImpl<TypeList<First const&, Rest...>, TypeList<Accum...>>
    : Inherit<
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First const&, true>>>,
          MatchSignatureImpl<
              TypeList<Rest...>,
              TypeList<Accum..., Tagged<First, false>>>> {};

template <typename... Args>
using MatchSignature = MatchSignatureImpl<TypeList<Args...>>;

}  // namespace match_signature_detail

template <typename Class, typename... Args>
constexpr auto match_signature() -> decltype(  //
    match_signature_detail::MatchSignature<Args...>::template match<
        invoke_result_t<Class, Args...>,
        Class>(&Class::operator(), match_signature_detail::Tag_v<Args>...)
) {
    return match_signature_detail::MatchSignature<Args...>::template match<
        invoke_result_t<Class, Args...>,
        Class>(&Class::operator(), match_signature_detail::Tag_v<Args>...);
}

}  // namespace impl
}  // namespace pbv

#endif  // PBV_IMPL_MATCH_SIGNATURE_HPP
