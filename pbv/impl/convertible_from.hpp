#ifndef PBV_IMPL_CONVERTIBLE_FROM_HPP
#define PBV_IMPL_CONVERTIBLE_FROM_HPP

#include <concepts>

namespace pbv {
namespace impl {

template <typename To, typename From>
concept convertible_from =
    std::same_as<To, From> ||
    requires(From (&start)(), void (&finish)(To)) { finish(start()); };

}  // namespace impl
}  // namespace pbv

#endif  // PBV_IMPL_CONVERTIBLE_FROM_HPP
