#ifndef PBV_IMPL_TYPE_LIST_HPP
#define PBV_IMPL_TYPE_LIST_HPP

namespace pbv {
namespace impl {

template <typename...>
struct TypeList {};

}  // namespace impl
}  // namespace pbv

#endif  // PBV_IMPL_TYPE_LIST_HPP
