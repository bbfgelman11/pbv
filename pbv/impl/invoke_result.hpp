#ifndef PBV_IMPL_INVOKE_RESULT_HPP
#define PBV_IMPL_INVOKE_RESULT_HPP

#include <type_traits>

namespace pbv {
namespace impl {

template <typename Class, typename... Args>
using invoke_result_t =
    decltype(std::declval<Class&>()(std::declval<Args (&)()>()()...));

}  // namespace impl
}  // namespace pbv

#endif  // PBV_IMPL_INVOKE_RESULT_HPP
