#ifndef PBV_FUNCTION_REF_HPP
#define PBV_FUNCTION_REF_HPP

#include "impl/convertible_from.hpp"
#include "impl/invoke_result.hpp"
#include "impl/match_signature.hpp"
#include <memory>
#include <type_traits>
#include <utility>

namespace pbv {
namespace detail {

template <typename T>
struct thunk_arg {
    using type = T&&;
};

template <typename T>
requires std::is_reference_v<T>
struct thunk_arg<T> {
    using type = T;
};

template <typename T>
requires std::is_trivially_copyable_v<T>
struct thunk_arg<T> {
    using type = T;
};

template <typename T>
using thunk_arg_t = typename thunk_arg<T>::type;

}  // namespace detail

template <auto V>
struct nontype_t {};

template <auto V>
inline constexpr nontype_t<V> nontype = {};

template <typename>
class function_ref;

template <typename R, typename... Args>
class function_ref<R(Args...)> {
    using thunk_t = R (*)(void*, detail::thunk_arg_t<Args>...);

    void* m_bound;
    thunk_t m_thunk;

    template <typename>
    struct free_fun;

    template <typename R2, typename... Args2>
    struct free_fun<R2(Args2...)> {
        static R thunk(void* bound, detail::thunk_arg_t<Args>... args) {
            using fun_t = R2 (*)(detail::thunk_arg_t<Args2>...);

            return reinterpret_cast<fun_t>(bound)(std::forward<Args>(args)...);
        }
    };

    template <auto>
    struct mem_fun;

    template <
        typename R2,
        typename Class,
        typename... Args2,
        R2 (Class::*MemFun)(Args2...)>
    struct mem_fun<MemFun> {
        static R thunk(void* bound, detail::thunk_arg_t<Args>... args) {
            using fun_t = R2 (Class::*)(detail::thunk_arg_t<Args2>...);
#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
            return (static_cast<Class*>(bound)->*reinterpret_cast<fun_t>(MemFun))(
                std::forward<Args>(args)...
            );
#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic pop
#endif
        }
    };

  public:
    template <typename R2, typename... Args2>
    requires(impl::convertible_from<Args2, Args> && ... &&
             impl::convertible_from<R, R2>)
    function_ref(R2 (*fun)(Args2...))
        : m_bound(reinterpret_cast<void*>(fun)),
          m_thunk(&free_fun<R2(Args2...)>::thunk) {}

    template <
        typename R2,
        typename Class,
        typename... Args2,
        R2 (Class::*MemFun)(Args2...)>
    requires(impl::convertible_from<Args2, Args> && ... &&
             impl::convertible_from<R, R2>)
    function_ref(nontype_t<MemFun>, Class& obj)
        : m_bound(std::addressof(obj)), m_thunk(&mem_fun<MemFun>::thunk) {}

    template <typename Class>
    requires  //
        impl::convertible_from<R, impl::invoke_result_t<Class, Args...>> &&
        requires() { impl::match_signature<Class, Args...>(); }
        function_ref(Class& obj)
        : function_ref(nontype<impl::match_signature<Class, Args...>()>, obj) {
    }

    R operator()(Args... args) const {
        return m_thunk(m_bound, std::forward<Args>(args)...);
    }
};

}  // namespace pbv

#endif  // PBF_FUNCTION_REF_HPP
