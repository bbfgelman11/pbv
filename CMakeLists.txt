cmake_minimum_required(VERSION 3.28)
project(pbv LANGUAGES CXX)

include(CTest)

add_library(pbv INTERFACE)
target_compile_features(pbv INTERFACE cxx_std_20)
target_sources(pbv
    PUBLIC FILE_SET HEADERS FILES
        pbv/function_ref.hpp
        pbv/impl/convertible_from.hpp
        pbv/impl/invoke_result.hpp
        pbv/impl/match_signature.hpp
        pbv/impl/type_list.hpp)

include(GNUInstallDirs)
install(TARGETS pbv EXPORT pbv FILE_SET HEADERS)
install(EXPORT pbv
    NAMESPACE pbv::
    DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/pbv")
install(FILES
    pbvConfig.cmake
    DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/pbv")

if(BUILD_TESTING)
add_subdirectory(tests)
endif()
