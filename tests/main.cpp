#include <pbv/function_ref.hpp>
#include <stdio.h>

namespace {

struct Mountain {
    char const* const name;

    Mountain(char const* name) noexcept : name(name) {}

    void climb() { printf("Climb %s\n", name); }

    Mountain(Mountain const&) = delete;
    Mountain(Mountain&&) = delete;

    ~Mountain() {}
};

struct Canyon {
    char const* const name;

    Canyon(char const* name) noexcept : name(name) {}

    void descend() { printf("Descend %s\n", name); }

    Canyon(Canyon const&) = delete;
    Canyon(Canyon&&) = delete;

    ~Canyon() {}
};

void normal_function(Mountain m) { m.climb(); }

struct NormalClass {
    void member_function(Mountain m) { m.climb(); }
};

struct FunctionObject {
    void operator()(Mountain m) { m.climb(); }
};

struct Explorer {
    void operator()(Mountain m, Canyon c) {
        m.climb();
        c.descend();
    }
};

}  // namespace

int main(int, char**) {
    {
        pbv::function_ref<void(Mountain)> fun(&normal_function);
        fun(Mountain("normal_function"));
    }
    {
        NormalClass obj;
        pbv::function_ref<void(Mountain)> fun(
            pbv::nontype<&NormalClass::member_function>, obj
        );
        fun(Mountain("NormalClass::member_function"));
    }
    {
        FunctionObject obj;
        pbv::function_ref<void(Mountain)> fun(obj);
        fun(Mountain("FunctionObject::operator()"));
    }
    {
        Explorer e;
        pbv::function_ref<void(Mountain, Canyon)> fun(e);
        fun(Mountain("Explorer::operator() Mountain"),
            Canyon("Explorer::operator() Canyon"));
    }
}
