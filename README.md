# `pbv`: Pass By Value

Pass objects by value without overhead.


```cpp

#include <pbv/function_ref.hpp>

struct Mountain {
  Mountain(Mountain const&) = delete;
  Mountain(Mountain&&) = delete;
  ~Mountain() {}
};

int main(int, char**) {
  auto lambda = [](Mountain) {};
  bpv::function_ref<void(Mountain)> fun(lambda);
  fun(Mountain{});
}
```
